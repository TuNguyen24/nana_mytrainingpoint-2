﻿Repository để lưu đồ án Nhập Môn Công Nghệ Phần Mềm Nhóm 2 Lớp 1.

* Tên đồ án: Website quản lý điểm rèn luyện cho sinh viên (MyTrainingPoint).

* Thành viên:
	- Lê Thị Thiên Trang
	- Lý Anh Vũ
	- Nguyễn Phan Minh Tú
	- Nguyễn Anh Tuấn
	- Đào Xuân Tin

* Mô hình sử dụng MVC
	- node_modules: Chứa các thư viện của node.(đã xóa)
	- Model: Chứa các hàm xử lý yêu cầu truy vấn csdl và yêu cầu. (Tin và Tú)
	- views: Chứa các trang html để thể hiện ra bên ngoài. (Trang và Tuấn)
	- controler: Chứa hàm chính để nhận yêu cầu từ Clent và gửi đến cho model xử lý và kết hợp với views để trả về cho cleint.
	- public: chứa các file công khai, ví dụ như image, css, js,...

* Cách Dùng: 	npm install
		node server.js
