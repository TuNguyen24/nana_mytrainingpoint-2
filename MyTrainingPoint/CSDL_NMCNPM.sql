create database MyTrainingPoint;

use MyTrainingPoint;

/*********************************/

create table SINHVIEN(
	MSSV char(7),
    CMND varchar(11),
    Ten nvarchar(30),
    DRL int,
    LoaiDiem nvarchar(10),
    MaKhoa char(5),
    MaLop varchar(10),
    NamNhapHoc int,
    SDT varchar(11),
    Email varchar(50),
    GioiTinh bit,
    NgaySinh date,
    NoiSinh nvarchar(50),
    DiaChi nvarchar(50),
    MatKhau varchar(30),
    
    primary key (MSSV)
);

create table HOATDONG(
	MaHD varchar(10),
    MaLoai char(2),
    Ten nvarchar(100),
    SoDRLDatDuoc int,
    NgayToChuc date,
    ThoiGian time,
    DiaDiemToChuc nvarchar(200),
    SoLuongThamGia int,
    
    primary key (MaHD)
);

create table DONVIPHUTRACH(
	MaHD varchar(10),
    Cap nvarchar(10),
    Ten nvarchar(50),
    
    primary key (MaHD,Cap)
);

create table LOAIHOATDONG(
	MaLoai char(2),
    Ten nvarchar(100),
    TongDiemToiDa int,
    
    primary key (MaLoai)
);

create table XEPLOAIDRL(
	SoDiem int,
    LoaiDiem nvarchar(10),
    
    primary key (SoDiem,LoaiDiem)
);

create table KHOA(
	MaKhoa char(5),
    Ten nvarchar(50),
    SDT varchar(11),
    Email varchar(50),
    Phong varchar(5),
    primary key (MaKhoa)
);

create table TROLISV(
	MaKhoa char(5),
    Ten nvarchar(50),
    SDT varchar(11),
    Email varchar(50),
    PhongLamViec varchar(5),
        
    primary key (MaKhoa,Ten)
);

create table THAMGIAHOATDONG(
	MSSV char(7),
    MaHD varchar(10),
    DaThamGia bit,
    
    primary key (MSSV,MaHD)
);

create table ADMIN(
	Ten nvarchar(50),
    NamSinh int, 
    AdminRoot bit,
    SuaSV bit,
    SuaHD bit,
    SuaAdmin bit,
    SuaTroLiSV bit,
    TaiKhoan varchar(30),
    MatKhau varchar(30),
    
    primary key (Ten)
);

/*Tạo khóa ngoại*/

alter table SINHVIEN add constraint FK_MaKhoa_SinhVien foreign key(MaKhoa) references KHOA(MaKhoa);

alter table SINHVIEN add constraint FK_SV_XLDRL foreign key(DRL,LoaiDiem) references XEPLOAIDRL(SoDiem,LoaiDiem);

alter table HOATDONG add constraint FK_MaLoai_HoatDong foreign key(MaLoai) references LOAIHOATDONG(MaLoai);

alter table DONVIPHUTRACH add constraint FK_MaHD_DonViPhuTrach foreign key(MaHD) references HOATDONG(MaHD);

alter table TROLISV add constraint FK_MaKhoa_TroLiSV foreign key(MaKhoa) references KHOA(MaKhoa);

alter table THAMGIAHOATDONG add constraint FK_Mssv_ThamGiaHoatDong foreign key(MSSV) references SINHVIEN(MSSV);

alter table THAMGIAHOATDONG add constraint FK_MaHD_ThamGiaHoatDong foreign key(MaHD) references HOATDONG(MaHD);

create procedure sp_DanhSachHoatDong (in inmssv char(7))
	select*, 
    (case
		when MaHD in (select tg.MaHD from thamgiahoatdong tg where tg.MSSV = inmssv) then 1
        else 0
    end) DaDangKy 
	from 
    (	select*
		from hoatdong hd 
		where hd.MaHD not in (select tg.MaHD from thamgiahoatdong tg where tg.DaThamGia = 1 and tg.MSSV = inmssv)
	) DSDaThamGia ;
