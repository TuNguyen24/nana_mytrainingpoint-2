//hàm trả về MaKhoa,MSSV,Ten của sinh viên
//cb có 2 đối số là (err,data)=>
exports.ThongTinSinhVien = function(req,cb){
    req.getConnection(function(err,connection){
        connection.query("select MaKhoa,MSSV,Ten from sinhvien where MSSV='"+req.user+"';",function(err,data){
            if(err){
                console.log(err);
                return cb(err,data);            
            }
            return cb(err,data);
        })
    })
}

//hàm trả về toàn bộ thông tin của sinh viên
//cb có 2 đối số là (err,data)=>
exports.ThongTinSinhVienChiTiet = function(req,cb){
    req.getConnection(function(err,connection){
        connection.query("select * from sinhvien where MSSV='"+req.user+"';",function(err,data){
            if(err){
                console.log(err);
                return cb(err,data);            
            }
            return cb(err,data);
        })
    })
}

//hàm trả về số điểm của sinh viên
//req: req của người dùng
//cb có 2 đối số, 1 là err 2 là số điểm trả về
exports.Diem = function(req,cb){
    var mssv = req.user;
    var tongDiem = 0;
    var diemThanhPhan = [0,0,0,0,0,0];
    req.getConnection(function(err,connection){
        connection.query("select * from thamgiahoatdong tg join hoatdong hd on tg.MaHD=hd.MaHD join loaihoatdong lhd on lhd.MaLoai = hd.MaLoai where tg.MSSV='"+mssv+"';",function(err,dshd){
            for(var j = 0;j<dshd.length;j++){ 
                if(dshd[j].DaThamGia[0] == 1){                 
                    if(diemThanhPhan[dshd[j].MaLoai - 1] +dshd[j].SoDRLDatDuoc <= dshd[j].TongDiemToiDa ){
                        diemThanhPhan[dshd[j].MaLoai - 1] += dshd[j].SoDRLDatDuoc;
                    }
                }
            }
            for(var i =0;i<6;i++){
                tongDiem += diemThanhPhan[i];
            }
            cb(null,tongDiem);
        })
    })
}

//hàm trả về danh sách các hoạt động mà sinh viên đó đã đăng kí
//req: là req của người dùng
//cb là hàm gồm có 2 đối số, 1 err: chứa các lỗi phát sinh
//                          2 data; danh sách hoạt động mà sinh viên đăng kí
exports.HoatDongDaDangKi = function(req,cb){
    req.getConnection(function(err,connection){
        connection.query("select tg.MSSV,hd.Ten,hd.MaHD,tg.DaThamGia from thamgiahoatdong tg join hoatdong hd on tg.MaHD=hd.MaHD where MSSV='"+req.user+"';",function(err,data){
            if(err){
                console.log(err);
                return cb(err,data);
            }
            else{
                return cb(err,data);
            }
        })
    })
}

//hàm trả về danh sách các hoạt động mà sinh viên đó đã đăng kí diễn ra trong 7 ngày tới
//req: là req của người dùng
//cb là hàm gồm có 2 đối số, 1 err: chứa các lỗi phát sinh
//                          2 data; danh sách hoạt động mà sinh viên đăng kí trong 7 ngày tới
exports.HoatDongSapToi = function(req,cb){
    req.getConnection(function(err,connection){
        connection.query("select hd.MaHD, hd.Ten from thamgiahoatdong tg join hoatdong hd on tg.MaHD=hd.MaHD where adddate(current_date(),7) >= NgayToChuc and tg.MSSV = '"+req.user+"';",function(err,data){
            if(err){
                console.log(err);
                return cb(err,data);
            }
            else{
                return cb(err,data);
            }
        })
    })
}

//hàm trả về danh sách các hoạt động
//req: là req của người dùng
//cb là hàm gồm có 2 đối số, 1 err: chứa các lỗi phát sinh
//                          2 data; danh sách hoạt động
exports.HoatDong = function(req,cb){
    req.getConnection(function(err,connection){
        connection.query("call sp_DanhSachHoatDong('"+req.user+"');",function(err,data){
            if(err){
                console.log(err);
                return cb(err,data);
            }
            else{
                return cb(err,data);
            }
        })
    })
}

//req là yêu cầu từ người dùng
//mahd là mã hoạt động cần xem chi tiết
//cb là hàm trả kết quả về, có 2 đối số, 1 là err: sẽ chứa lỗi khi có lỗi
//                                        2 là data: sẽ chứ data khi truy vấn thành công
exports.HoatDongChiTiet = function(req,mahd,cb){
    req.getConnection(function(err,connection){
        var query = connection.query("select* from hoatdong where MaHD='"+mahd+"';",function(err,data){
            if(err){
                console.log(err);
            }
            cb(err,data);
        })
    })
}

//req là yêu cầu từ người dùng
//cb là hàm trả về, có 2 đối số, 1 là err: sẽ chứa lỗi khi có lỗi
//                                  2 là data: sẽ chứa data khi truy vấn thành công
exports.ThamGiaHD = function(req,cb){
    req.getConnection(function(err,connection){
        connection.query("select tg.MaHD,hd.Ten,hd.MaLoai,(case when DaThamGia is NULL then 0 when DaThamGia = 1 then 1 else 0 end) DaThamGia from thamgiahoatdong tg join hoatdong hd on tg.MaHD=hd.MaHD where MSSV='"+req.user+"';",
        function(err,data){
            cb(err,data);
            return;
        })
    })
}